# ICP

#### 构成

**Matlab** 文件夹中:
- "bunny.m" 为用MATLAB实现的ICP匹配，未用Kd-tree
- "bun_zipper.txt" 为兔子顶点的集合，每排前三位为x, y, z，后两位本算法用不到
- "paint.m" 用于在MATLAB画图
- "rotate.m" 用于 "bunny.m"

**build** 文件夹中为编译好的程序，如有需要请删去重新编译

**ICP.cpp** : 定义了用于ICP匹配的相关函数，计算两点距离、暴力寻找最临近点、旋转点集（用于生成数据）、运动估计（用kd-tree）

**kdTree.h** : 定义了kd-tree类

**main.cpp** : 读取数据，生成数据，并用ICP算法计算运动
