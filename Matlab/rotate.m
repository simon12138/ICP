function [data_q,T] = rotate(data,R,t)
   
T = [R'; t]';          %旋转矩阵
 
rows=size(data,2);
rows_one=ones(1,rows);
data=[data;rows_one];    %化为齐次坐标
 
data_q=T*data;    %返回三维坐标