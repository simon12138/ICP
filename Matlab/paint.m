clear;
close all;
clc;

fileA1 = load("../build/initialA.dat");
fileB1 = load("../build/initialB.dat");

data_source=fileA1';
data_target=fileB1';

x1=data_source(1,:);
y1=data_source(2,:);
z1=data_source(3,:);
x2=data_target(1,:);
y2=data_target(2,:);
z2=data_target(3,:);

figure;
scatter3(x1,y1,z1,'b.'),axis equal
hold on;
scatter3(x2,y2,z2,'r.');
hold off;
clear;

fileA2 = load("../build/finalA.dat");
fileB2 = load("../build/finalB.dat");

data_source=fileA2';
data_target=fileB2';

x1=data_source(1,:);
y1=data_source(2,:);
z1=data_source(3,:);
x2=data_target(1,:);
y2=data_target(2,:);
z2=data_target(3,:);

figure;
scatter3(x1,y1,z1,'b.'),axis equal
hold on;
scatter3(x2,y2,z2,'r.');
hold off;